import configparser
import selenium.webdriver
import os, re, time, sys

Config = configparser.ConfigParser()
Post, Prepare, Test, Draft = False, False, False, False
PrepareNZB, PrepareCovers, Headless = False, False, False

ForumID = -1

IniDir, CoverDir, ExtraDir, NZBDir, ConfigFile = "upload/", "upload/cover/", "upload/extra/", "upload/nzbs/", "config.ini"

try:
	Config.read(ConfigFile)
except:
	kill = True
	for arg in sys.argv:
		if arg.startswith("--config="):
			kill = False
	if kill:
		print("Error reading default Configuration")
		exit(1)

if len(sys.argv) == 1:
	print("Poster requires at least one Argument")
	print("Use --help for usage information")
	exit(2)

if sys.argv[1] == "--help":
	print("python poster.py [ARGUMENTS]")
	print("Modi:")
	print("\t--post\t\t\tPost to HoU")
	print("\t--prepare\t\tCreate Placeholder Files")
	print("\t--test\t\t\tCheck if there are any errors with the")
	print("\t\t\t\tPrepared Files")
	print("Options:")
	print("\t--ini=[File]\t\tChange used post configuration")
	print("\t\t\t\t(Default: upload/)")
	print("\t--cover=[File]\t\tChange used cover folder")
	print("\t\t\t\t(Default: upload/cover/)")
	print("\t--extra=[File]\t\tChange used extra folder")
	print("\t\t\t\t(Default: upload/extra/)")
	print("\t--nzb=[File]\t\tChange used NZB folder")
	print("\t\t\t\t(Default: upload/nzbs/)")
	print("\t--config=[File]\t\tChange used configuration")
	print("\t\t\t\t(Default: config.ini)")
	print("\t--draft\t\t\tSave Posts as draft instead of posting")
	print("\t--nzbs\t\t\tPrepare Posts by nzb folder content")
	print("\t--covers\t\tPrepare Posts by covers folder content")
	print("\t--headless\t\tRun in headless mode (no browser window)")
	exit()

for arg in sys.argv:
	if arg == "--post":
		Post = True
	elif arg == "--prepare":
		Prepare = True
	elif arg == "--test":
		Test = True
	elif arg.startswith("--ini="):
		IniDir = arg.replace("--ini=", "")
	elif arg.startswith("--cover="):
		CoverDir = arg.replace("--cover=", "")
	elif arg.startswith("--extra="):
		ExtraDir = arg.replace("--extra=", "")
	elif arg.startswith("--nzb="):
		NZBDir = arg.replace("--nzb=", "")
	elif arg.startswith("--config="):
		Config.read(arg.replace("--config=", ""))
	elif arg == "--draft":
		Draft = True
	elif arg == "--nzbs":
		PrepareNZB = True
	elif arg == "--covers":
		PrepareCovers = True
	elif arg == "--headless":
		Headless = True

Config.set('Folders', 'Inidir', IniDir)
Config.set('Folders', 'Coverdir', CoverDir)
Config.set('Folders', 'Extradir', ExtraDir)
Config.set('Folders', 'NZBdir', NZBDir)

c = 0
if Post:
	c += 1
if Prepare:
	c += 1
if Test:
	c += 1

if c != 1:
	print("Exactly one Mode is required")
	exit(2)

'''Preparations'''
if Prepare:
	print("Preparing INI Files...")
	src = "NZBs"
	if PrepareCovers:
		src = "covers"

	print("Using " + src + " as source")
	arr = NZBDir
	if PrepareCovers:
		arr = CoverDir

	regex = r"{([^\}])*}"
	matches = re.finditer(regex, open(Config.get('Other', 'Template')).read(), re.MULTILINE)
	fields = ["NZB", "Cover"]
	for match in matches:
		fields.append(match.group())

	Files = 0
	inistr = "[Post]\r\n"
	for entry in fields:
		if entry == "{Cover-Link}" or entry == "{NZB-Link}":
			continue
		inistr += entry.replace("{", "").replace("}", "") + " = "
		if entry == "Cover":
			inistr += "[cover]"
		if entry == "NZB":
			inistr += "[nzb]"
		if entry.startswith("{File"):
			Files += 1
			inistr += "[name]_" + entry.replace("{File", "").replace("}", "") + str(Files) + ".txt"

		inistr += "\r\n"

	for file in os.listdir(arr):
		if PrepareNZB:
			if Files > 0:
				for i in range(1, Files + 1):
					open(ExtraDir + file + str(i) + ".txt", 'a').write(" ")
			open(IniDir + file + ".ini", 'a').write(
				inistr.replace("[name]", ExtraDir + file).replace("[cover]", "").replace("[nzb]", NZBDir + file))
		elif PrepareCovers:
			if Files > 0:
				for i in range(1, Files + 1):
					open(ExtraDir + file + str(i) + ".txt", 'a').write(" ")
			open(IniDir + file + ".ini", 'a').write(
				inistr.replace("[name]", ExtraDir + file).replace("[cover]", ExtraDir + file).replace("[nzb]", ""))
		inistr += entry.replace("{", "").replace("}", "") + " = "
	print("Preparations completed")
elif Post:
	'''Post'''
	SBrowser = Config.get('Browser', 'Browserchoice')

	print("Setting Up Browser")
	opts = selenium.webdriver.FirefoxOptions()
	if SBrowser == "firefox":
		opts = selenium.webdriver.FirefoxOptions()
	elif SBrowser == "chrome":
		opts = selenium.webdriver.ChromeOptions()
	opts.binary_location = Config.get('Browser', 'BrowserBinary')

	if Headless:
		opts.headless = True

	print("Starting " + Config.get('Browser', 'BrowserBinary'))
	if SBrowser == "firefox":
		browser = selenium.webdriver.Firefox(options=opts)
	elif SBrowser == "chrome":
		browser = selenium.webdriver.Chrome(options=opts)

	print("Loading the House")
	browser.get('https://house-of-usenet.ru/index.php')

	rememberme_checkbox = browser.find_element_by_name("remember")
	rememberme_checkbox.click()
	username_textbox = browser.find_element_by_name("username")
	username_textbox.send_keys(Config.get('User', 'Username'))
	password_textbox = browser.find_element_by_name("password")
	password_textbox.send_keys(Config.get('User', 'Password'))
	password_textbox.submit()

	time.sleep(3)

	try:
		browser.find_element_by_name('newshout')
	except selenium.common.exceptions.NoSuchElementException:
		print('Login not successful')
		exit(1)

	print("Logged in successfully")
	print("Starting to post")

	for filename in os.listdir('upload'):
		if not filename.endswith('.ini'):
			continue
		browser.get('https://house-of-usenet.ru/newthread.php?fid=' + Config.get('Forum', 'ForumID'))

		cover_image = browser.find_element_by_name('xthreads_timg')
		source_button = browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[6]/td[2]/div/div[1]/div[8]/a[2]')
		post = browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[6]/td[2]/div/textarea')
		nzb = browser.find_element_by_name('attachments[]')

		postconfig = configparser.ConfigParser()
		postconfig.read('upload/' + filename)

		print('Uploading Files')

		cover_image.send_keys(os.path.abspath(os.curdir) + "/upload/" + postconfig.get('Post', 'Cover'))
		nzb.send_keys(os.path.abspath(os.curdir) + '/upload/' + postconfig.get('Post', 'NZB'))
		source_button.click()
		post.send_keys('Platzhalter')
		browser.find_element_by_name('newattachment').click()

		print("Grabbing Links")
		cover_link = browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[4]/td[2]/table/tbody/tr/td/div/div/a').get_property(
			'href')
		postconfig.set('Post', 'cover-link', cover_link)
		nzb_link = browser.find_element_by_name('insert').get_attribute('onclick').replace(
			"$('#message').sceditor('instance').insertText('", '').replace("'); return false;", '')
		postconfig.set('Post', 'nzb-link', nzb_link)

		print('Writing Post')
		browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[6]/td[2]/div/div[1]/div[8]/a[2]').click()
		post = browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[6]/td[2]/div/textarea')
		post.clear()

		poststring = open('template').read()

		regex = r"{([^\}])*}"

		matches = re.finditer(regex, poststring, re.MULTILINE)

		subject_text = browser.find_element_by_name('subject')
		subject_text.send_keys(postconfig.get('Post', 'subject'))
		for match in enumerate(matches):
			subject_text = browser.find_element_by_name('subject')
			if match.group().startswith('{File'):
				postconfig.set('Post', match.group().replace("{", "").replace("}", ""), open(
					'upload/' + postconfig.get('Post', match.group().replace("{", "").replace("}", ""))).read()), open(
					'upload/' + postconfig.get('Post', match.group().replace("{", "").replace("}", "")).read())
				poststring = poststring.replace(match.group(),
				                                postconfig.get('Post', match.group().replace("{", "").replace("}", "")))
				post.send_keys(poststring)

				if Draft:
					browser.find_element_by_name('savedraft').click()
				else:
					browser.find_element_by_xpath('/html/body/div/div[3]/div/div/form/div/input[1]').click()
	browser.close()
elif Test:
	SBrowser = Config.get('Browser', 'Browserchoice')

	print("Setting Up Browser")
	opts = selenium.webdriver.FirefoxOptions()
	if SBrowser == "firefox":
		opts = selenium.webdriver.FirefoxOptions()
	elif SBrowser == "chrome":
		opts = selenium.webdriver.ChromeOptions()
	opts.binary_location = Config.get('Browser', 'BrowserBinary')

	if Headless:
		opts.headless = True

	print("Starting " + Config.get('Browser', 'BrowserBinary'))
	if SBrowser == "firefox":
		browser = selenium.webdriver.Firefox(options=opts)
	elif SBrowser == "chrome":
		browser = selenium.webdriver.Chrome(options=opts)

	print("Loading the House")
	browser.get('https://house-of-usenet.ru/index.php')

	rememberme_checkbox = browser.find_element_by_name("remember")
	rememberme_checkbox.click()
	username_textbox = browser.find_element_by_name("username")
	username_textbox.send_keys(Config.get('User', 'Username'))
	password_textbox = browser.find_element_by_name("password")
	password_textbox.send_keys(Config.get('User', 'Password'))
	password_textbox.submit()

	time.sleep(3)

	try:
		browser.find_element_by_name('newshout')
	except selenium.common.exceptions.NoSuchElementException:
		print('ERROR FOUND: Login not successful')
		exit(1)
	finally:
		browser.close()
	print("Logged in successfully")
	print("Starting to post")

	for filename in os.listdir('upload'):
		if not filename.endswith('.ini'):
			continue
		browser.get('https://house-of-usenet.ru/newthread.php?fid=' + Config.get('Forum', 'ForumID'))

		cover_image
		try:
			cover_image = browser.find_element_by_name('xthreads_timg')
		except:
			print("ERROR FOUND: Posting in Forum " + Config.get('Forum', 'ForumID') + " is not allowed")
			exit(1)
		finally:
			browser.close()
		source_button = browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[6]/td[2]/div/div[1]/div[8]/a[2]')
		post = browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[6]/td[2]/div/textarea')
		nzb = browser.find_element_by_name('attachments[]')

		postconfig = configparser.ConfigParser()
		try:
			postconfig.read('upload/' + filename)
		except:
			print("ERROR FOUND: File " + filename + " is not readable")
			exit(1)
		finally:
			browser.close()

		print('Uploading Files')
		try:
			cover_image.send_keys(os.path.abspath(os.curdir) + "/upload/" + postconfig.get('Post', 'Cover'))
		except:
			print("ERROR FOUND: Cover not found under: " + os.path.abspath(os.curdir) + "/upload/" + postconfig.get(
				'Post', 'Cover'))
			exit(1)
		finally:
			browser.close()

		try:
			nzb.send_keys(os.path.abspath(os.curdir) + '/upload/' + postconfig.get('Post', 'NZB'))
		except:
			print(
				"ERROR FOUND: NZB not found under: " + os.path.abspath(os.curdir) + "/upload/" + postconfig.get('Post',
				                                                                                                'NZB'))
			exit(1)
		finally:
			browser.close()
		source_button.click()
		post.send_keys('Platzhalter')
		browser.find_element_by_name('newattachment').click()

		print("Grabbing Links")
		try:
			cover_link = browser.find_element_by_xpath(
				'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[4]/td[2]/table/tbody/tr/td/div/div/a').get_property(
				'href')
		except:
			print("ERROR FOUND: Can't retrieve Cover-Link")
			exit(1)
		finally:
			browser.close()

		postconfig.set('Post', 'cover-link', cover_link)
		try:
			nzb_link = browser.find_element_by_name('insert').get_attribute('onclick').replace(
				"$('#message').sceditor('instance').insertText('", '').replace("'); return false;", '')
		except:
			print("ERROR FOUND: Can't retrieve NZB-Link")
			exit(1)
		finally:
			browser.close()
		postconfig.set('Post', 'nzb-link', nzb_link)

		print('Writing Post')
		browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[6]/td[2]/div/div[1]/div[8]/a[2]').click()
		post = browser.find_element_by_xpath(
			'/html/body/div/div[3]/div/div/form/table[1]/tbody/tr[6]/td[2]/div/textarea')
		post.clear()

		poststring = open('template').read()

		regex = r"{([^\}])*}"

		matches = re.finditer(regex, poststring, re.MULTILINE)

		subject_text = browser.find_element_by_name('subject')
		subject_text.send_keys(postconfig.get('Post', 'subject'))
		for match in enumerate(matches):
			subject_text = browser.find_element_by_name('subject')
			if match.group().startswith('{File'):
				postconfig.set('Post', match.group().replace("{", "").replace("}", ""), open(
					'upload/' + postconfig.get('Post', match.group().replace("{", "").replace("}", ""))).read()), open(
					'upload/' + postconfig.get('Post', match.group().replace("{", "").replace("}", "")).read())
				poststring = poststring.replace(match.group(),
				                                postconfig.get('Post', match.group().replace("{", "").replace("}", "")))
				post.send_keys(poststring)

	browser.close()
	print("TEST SUCCESSFUL")
	print("No errors found")
	exit(1)